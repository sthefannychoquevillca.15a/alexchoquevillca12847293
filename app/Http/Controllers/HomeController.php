<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\formularioModel;
use Illuminate\Support\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function listado(){
        $datos = formularioModel:: paginate (10);
        'listado'=>$datos
        ]);

    }

    public function listarUsuarios(){

        return formularioModel::cursor();

    }

    public  function listadoApiFind(formularioModel $formulario){

        return $formulario;

    }

    public  function listadoFind(formularioModel $formulario){

        return view('new.find')->with([
            'find'=>$formulario
        ]);

    }


   

    public function salir(){
        Auth::logout();

        return redirect()->route('login');
    }
}
